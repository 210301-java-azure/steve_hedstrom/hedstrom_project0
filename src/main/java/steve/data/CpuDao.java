package steve.data;

import steve.models.CPU;

import java.util.List;

public interface CpuDao {
    List<CPU> getAllCpus();
    List<CPU> getCpuInPriceRange(double min, double max);
    CPU getCpuById(int cpuId);
    List<CPU> getCpuByCores(int cores);
    void addNewCpu(CPU cpu);
    void deleteCpu(int cpuId);
    void updateCpu(CPU cpu);
}

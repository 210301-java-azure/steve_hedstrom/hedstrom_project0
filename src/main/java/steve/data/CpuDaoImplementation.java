package steve.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.models.CPU;
import steve.util.ConnectionUtil;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CpuDaoImplementation implements CpuDao {
    private final Logger logger = LoggerFactory.getLogger(CpuDaoImplementation.class);
    private static final String ERRORSTR = "{} {}";

    @Override
    public List<CPU> getAllCpus() {
        List<CPU> cpuList = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection(); Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery("select * from cpu")) {
            while (resultSet.next()) {
                int cpuId = resultSet.getInt("cpu_id");
                String cpuName = resultSet.getString("cpu_name");
                int cores = resultSet.getInt("cores");
                double ghz = resultSet.getDouble("ghz");
                double price = resultSet.getDouble("price");
                CPU cpu = new CPU(cpuId, cpuName, cores, ghz, price);
                cpuList.add(cpu);
                logger.info("adding CPU ID: {} to the ArrayList", cpuId);
            }
            logger.info("selecting all CPUs from db - {} CPUs received", cpuList.size());

        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return cpuList;
    }

    @Override
    public CPU getCpuById(int cpuId) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("select * from cpu where cpu_id = ?")) {
            preparedStatement.setInt(1, cpuId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String cpuName = resultSet.getString("cpu_name");
                int cores = resultSet.getInt("cores");
                double ghz = resultSet.getDouble("ghz");
                double price = resultSet.getDouble("price");
                logger.info("1 CPU received from database by id: {}", cpuId);
                return new CPU(cpuId, cpuName, cores, ghz, price);
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<CPU> getCpuByCores(int cores) {
        List<CPU> cpuList = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("select * from cpu where cores = ?")) {
            preparedStatement.setInt(1, cores);
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
                int cpuId=resultSet.getInt("cpu_id");
                String cpuName = resultSet.getString("cpu_name");
                double ghz = resultSet.getDouble("ghz");
                double price = resultSet.getDouble("price");
                CPU cpu = new CPU(cpuId, cpuName, cores, ghz, price);
                cpuList.add(cpu);
                logger.info("adding CPU ID: {} to the ArrayList", cpuId);
            }
            logger.info("There are {} CPUs that have {} cores", cpuList.size(), cores);
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return cpuList;
    }

    @Override
    public void addNewCpu(CPU cpu) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("insert into cpu (cpu_id, cpu_name, cores, ghz, price) values (?, ?, ?, ?, ?)")) {
            preparedStatement.setInt(1, cpu.getCpuId());
            preparedStatement.setString(2, cpu.getCpuName());
            preparedStatement.setInt(3, cpu.getCores());
            preparedStatement.setDouble(4, cpu.getGhz());
            preparedStatement.setDouble(5, cpu.getPrice());
            preparedStatement.executeUpdate();
            logger.info("successfully added a new CPU to the database");
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCpu(int cpuId) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("delete from cpu where cpu_id=?")) {
            preparedStatement.setInt(1, cpuId);
            preparedStatement.executeUpdate();
            logger.warn("successfully deleted cpu {} from the database", cpuId);
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void updateCpu(CPU cpu) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("update cpu set cpu_name=?, cores=?, ghz=?, price=? where cpu_id=?")) {
            preparedStatement.setString(1, cpu.getCpuName());
            preparedStatement.setInt(2, cpu.getCores());
            preparedStatement.setDouble(3, cpu.getGhz());
            preparedStatement.setDouble(4, cpu.getPrice());
            preparedStatement.setInt(5, cpu.getCpuId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }

    public List<CPU> getCpuInPriceRange(double min, double max) {
        List<CPU> cpuList=new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("select * from cpu where (price>?) and (price<?)")) {
            preparedStatement.setDouble(1, min);
            preparedStatement.setDouble(2, max);
            logger.info("result set is executing query");
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
                int cpuId=resultSet.getInt("cpu_id");
                String cpuName=resultSet.getString("cpu_name");
                int cores=resultSet.getInt("cores");
                double mhz=resultSet.getDouble("ghz");
                double price=resultSet.getDouble("price");
                logger.info("adding CPU to the list");
                cpuList.add(new CPU(cpuId, cpuName, cores, mhz, price));
            }
            if(cpuList.isEmpty()){
                logger.error("No CPUs were added to the ArrayList, so nothing will be returned");
            }
            logger.info("closing result set");
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        logger.info("returning data");
        return cpuList;
    }
}
package steve.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.models.Customer;
import steve.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImplementation implements CustomerDao {
    private final Logger logger = LoggerFactory.getLogger(CustomerDaoImplementation.class);
    private static final String ERRORSTR = "{} {}";

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customerList = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection(); Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery("select customer_id, username, password, admin from customers")) {
            while (resultSet.next()) {
                int customerId = resultSet.getInt("customer_id");
                String username = resultSet.getString("username");
                boolean admin = resultSet.getBoolean("admin");
                Customer customer = new Customer(customerId, username, admin);
                customerList.add(customer);
            }
            logger.info("selecting all Customers from db - {} Customers received", customerList.size());

        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public Customer getCustomerById(int customerId) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("select customer_id, username, admin from customers where customer_id = ?")) {
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String username = resultSet.getString("username");
                boolean admin = resultSet.getBoolean("admin");
                logger.info("Customer received from database by id: {}", customerId);
                return new Customer(customerId, username, admin);
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Customer getCustomerByUsername(String username) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("select customer_id, username, admin from customers where username = ?")) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int customerId = resultSet.getInt("customer_id");
                boolean admin = resultSet.getBoolean("admin");
                logger.info("Customer received from database by Username: {}", username);
                return new Customer(customerId, username, admin);
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addNewCustomer(Customer customer) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("insert into customers (customer_id, username, password, admin) values (?, ?, ?, ?)")) {
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getUserName());
            preparedStatement.setString(3, customer.getPassword());
            preparedStatement.setBoolean(4, customer.isAdmin());
            preparedStatement.executeUpdate();
            logger.info("successfully added a new Customer to the database");
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCustomer(int customerId) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("delete from customers where customer_id=?")) {
            preparedStatement.setInt(1, customerId);
            preparedStatement.executeUpdate();
            logger.warn("successfully deleted customer {} from the database", customerId);
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void updateCustomerUsername(String customer, int customerId) {
        try (Connection connection = ConnectionUtil.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("update customers set username=? where customer_id=?")) {
            preparedStatement.setString(1, customer);
            preparedStatement.setInt(2, customerId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(ERRORSTR, e.getClass(), e.getMessage());
            e.printStackTrace();
        }
    }
}
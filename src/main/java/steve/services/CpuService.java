package steve.services;

import io.javalin.http.BadRequestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.data.CpuDao;
import steve.data.CpuDaoImplementation;
import steve.models.CPU;
import java.util.List;

public class CpuService {
    private final CpuDao cpuDao=new CpuDaoImplementation();
    private final Logger logger= LoggerFactory.getLogger(CpuService.class);

    public List<CPU> getAllCpus(){
        return cpuDao.getAllCpus();
    }
    public List<CPU> getCpuByCores(int cores){
        return cpuDao.getCpuByCores(cores);
    }
    public List<CPU> getCpuInPriceRange(String min, String max){
        logger.info("checking for null values");
        if(min!=null&&max!=null){
            logger.info("checking price format");
            if(matchesPriceFormat(min)&&matchesPriceFormat(max)){
                logger.info("returning cpuDao");
                return cpuDao.getCpuInPriceRange(Double.parseDouble(min), Double.parseDouble(max));
            }
            throw new BadRequestResponse("improper price format");
        }
        throw new BadRequestResponse("no null price values allowed");
    }
    public CPU getCpuById(int cpuId){
        return cpuDao.getCpuById(cpuId);
    }
    public void addNewCpu(CPU cpu){
        cpuDao.addNewCpu(cpu);
    }
    public void deleteCpu(int cpuId){
        cpuDao.deleteCpu(cpuId);
    }
    public void updateCpu(CPU cpu){
        cpuDao.updateCpu(cpu);
    }
    private boolean matchesPriceFormat(String str){
        return str.matches("\\d{0,4}(\\.\\d{1,2})?");
    }
}

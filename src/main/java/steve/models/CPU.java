package steve.models;

import java.io.Serializable;
import java.util.Objects;

public class CPU implements Serializable {
    private int cpuId;
    private String cpuName;
    private int cores;
    private double ghz;
    private double price;

    public CPU(){

    }

    public CPU(int cpuId, String cpuName, int cores, double ghz, double price) {
        this.cpuId = cpuId;
        this.cpuName = cpuName;
        this.cores = cores;
        this.ghz = ghz;
        this.price = price;
    }

    public int getCpuId() {
        return cpuId;
    }

    public void setCpuId(int cpuId) {
        this.cpuId = cpuId;
    }

    public String getCpuName() {
        return cpuName;
    }

    public void setCpuName(String cpuName) {
        this.cpuName = cpuName;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }

    public double getGhz() {
        return ghz;
    }

    public void setGhz(double mhz) {
        this.ghz = mhz;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CPU cpu = (CPU) o;
        return cpuId == cpu.cpuId && cores == cpu.cores && Double.compare(cpu.ghz, ghz) == 0 && Double.compare(cpu.price, price) == 0 && Objects.equals(cpuName, cpu.cpuName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpuId, cpuName, cores, ghz, price);
    }

    @Override
    public String toString() {
        return "CPU{" +
                "cpuId=" + cpuId +
                ", cpuName='" + cpuName + '\'' +
                ", cores=" + cores +
                ", mhz=" + ghz +
                ", price=" + price +
                '}';
    }

}

package steve;

import io.javalin.Javalin;
import steve.controllers.AuthenticationController;
import steve.controllers.CpuController;
import steve.controllers.CustomerController;
import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    AuthenticationController authenticationController = new AuthenticationController();
    CpuController cpuController=new CpuController();
    CustomerController customerController=new CustomerController();
    public JavalinApp() {
        Javalin app=Javalin.create().start();
        app.routes(() -> {
            path("cpu", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetAllCpus);
                post(cpuController::handlePostNewCpu);
                patch(cpuController::handleUpdateCpuById);
                path(":cpu_id", ()->{
                    before("", authenticationController::authorizeToken);
                    get(cpuController::handleGetCpuById);
                    delete(cpuController::handleDeleteCpuById);
                });
            });

            path("cpu-cores/:cores", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetCpuByCores);
            });

            path("cpu_price", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetCpuInPriceRange);
            });

            path("customers", ()->{
                before("", authenticationController::authorizeToken);
                get(customerController::handleGetAllCustomers);
                post(customerController::handlePostNewCustomer);
                patch(customerController::handleUpdateCustomerUsername);
                path(":customer_id", ()->{
                    before("", authenticationController::authorizeToken);
                    get(customerController::handleGetCustomerById);
                    delete(customerController::handleDeleteCustomerById);
                });
                path(":username", ()->{
                    before("", authenticationController::authorizeToken);
                    get(customerController::handleGetCustomerByUsername);
                });
            });

            path("login", () -> post(authenticationController::authenticateLogin));

        });
    }
}
# Computer Parts ECommerce
Computer Parts ECommerce is a RESTful API that provides back-end support for companies that are looking to sell their computer parts.
## Installation
Clone from this git.
## Usage
```python
GET http://localhost:7000/cpu # returns all CPUs
GET http://localhost:7000/cpu/:cpu_id # returns a specific CPU by its ID
GET http://localhost:7000/cpu-cores/:cores # returns a list of CPUs based on amount of cores
GET http://localhost:7000/cpu/price # (in-development) returns a list of CPUs between a price range based on form parameters
GET http://localhost:7000/customers # returns all Customers
GET http://localhost:7000/customers/:customer_id # returns a specific Customer by their ID

POST http://localhost:7000/cpu # creates a new CPU based on body parameters
POST http://localhost:7000/customers # creates a new Customer based on body parameters

PATCH http://localhost:7000/cpu # updates a CPU based on body parameters
PATCH http://localhost:7000/customer # updates a Customer based on body parameters

DELETE http://localhost:7000/cpu/:cpu_id # removes a CPU based on its ID
DELETE http://localhost:7000/customer/:customer_id # removes a Customer based on their ID
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
## License
Computer Parts ECommerce provides back-end support for selling computer parts.
Copyright (C) <2021> Steven S. Hedstrom

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.